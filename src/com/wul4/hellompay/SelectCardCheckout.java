package com.wul4.hellompay;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollView;
import com.wul4.hellompay.baseadapter.CheckoutFinishedAdapter;

public class SelectCardCheckout extends Activity {

    private Context context = null;

    private ArrayList<Card> mCards = null;
    private CardScrollView mCardScrollView = null;
    private float price = 0;

    @Override
    public void onCreate(Bundle saveInstance) {
        super.onCreate(saveInstance);
        context = this;
        
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            price = extras.getFloat("price");
        }
        
        mountCardList(price);
        
        mCardScrollView = new CardScrollView(context);
		CheckoutFinishedAdapter adapter = new CheckoutFinishedAdapter(mCards, context, price);
        mCardScrollView.setAdapter(adapter);
        mCardScrollView.activate();

        setContentView(mCardScrollView);
        mCardScrollView.setOnItemClickListener(adapter);
    }

    public void mountCardList(float price) {

        mCards = new ArrayList<Card>();
        Card card = new Card(context);
        card.addImage(R.drawable.card1);
        card.setText(R.string.card1);
        card.setFootnote(getString(R.string.total_price) + ": " + price);
        card.setImageLayout(Card.ImageLayout.FULL);
        mCards.add(card);

        Card card2 = new Card(context);
        card2.addImage(R.drawable.card2);
        card2.setText(R.string.card2);
        card2.setFootnote(getString(R.string.total_price) + ": " + price);
        card2.setImageLayout(Card.ImageLayout.FULL);
        mCards.add(card2);

        Card card3 = new Card(context);
        card3.addImage(R.drawable.card3);
        card3.setText(R.string.card3);
        card3.setFootnote(getString(R.string.total_price) + ": " + price);
        card3.setImageLayout(Card.ImageLayout.FULL);
        mCards.add(card3);
    }
}