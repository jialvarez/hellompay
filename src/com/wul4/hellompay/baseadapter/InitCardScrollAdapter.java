package com.wul4.hellompay.baseadapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollAdapter;
import com.wul4.hellompay.MenuActivity;

public class InitCardScrollAdapter extends CardScrollAdapter implements OnItemClickListener {

    private ArrayList<Card> mCards;
    private Context context = null;

    public InitCardScrollAdapter(ArrayList<Card> mCards, Context context) {
        this.context = context;
        this.mCards = mCards;
    }

    public InitCardScrollAdapter(ArrayList<Card> mCards, Context context, String productId) {
        this.context = context;
        this.mCards = mCards;
    }

    @Override
    public int getCount() {
        return mCards.size();
    }

    @Override
    public Object getItem(int position) {
        return mCards.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return mCards.get(position).getView();
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
        //        Intent intent = new Intent(context, MenuActivity.class);
        //        intent.putExtra("productId", productId.toString());            
        //        context.startActivity(intent);
    }

	@Override
	public int getPosition(Object item) {
		// TODO Auto-generated method stub
		return mCards.indexOf(item);
	}
}
