package com.wul4.hellompay.baseadapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollAdapter;
import com.wul4.hellompay.MenuActivity;
import com.wul4.hellompay.SelectCardCheckout;
import com.wul4.hellompay.SelectedCard;

public class CheckoutFinishedAdapter extends CardScrollAdapter implements OnItemClickListener {

    private ArrayList<Card> mCards;
    private Context context;
    private float price;

    public CheckoutFinishedAdapter(ArrayList<Card> mCards, Context context, float price) {
        this.mCards = mCards;
        this.context = context;
        this.price = price;
    }

    @Override
    public int getCount() {
        return mCards.size();
    }

    @Override
    public Object getItem(int position) {
        return mCards.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return mCards.get(position).getView();
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
    	Intent intent = null;
    	
    	intent = new Intent(context, SelectedCard.class);
        intent.putExtra("price", this.price);
        Card card = (Card) mCards.get(position);
        intent.putExtra("card", card.getText());
    	
        context.startActivity(intent);
        ((Activity) context).finish();
    }

	@Override
	public int getPosition(Object item) {
		// TODO Auto-generated method stub
		return mCards.indexOf(item);
	}
}
