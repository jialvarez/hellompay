package com.wul4.hellompay.util;

import com.wul4.hellompay.R;

public class Util {
    public static int getProductImage(String productId) {
        if (productId.contentEquals("1000")) {
            return R.drawable.nexus_1;
        } else if (productId.contentEquals("2000")) {
            return R.drawable.icecream;
        } else if (productId.contentEquals("3000")) {
            return R.drawable.pizza;
        } else if (productId.contentEquals("4000")) {
            return R.drawable.book;
        } else if (productId.contentEquals("5000")) {
            return R.drawable.cinematickets;
        }

        return R.drawable.nexus_1;
    }
}
