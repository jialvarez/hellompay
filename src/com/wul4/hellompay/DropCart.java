package com.wul4.hellompay;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollView;
import com.wul4.hellompay.baseadapter.DropCardScrollAdapter;
import com.wul4.hellompay.basedatos.DBProducts;

public class DropCart extends Activity {

    private static Context context = null;
    private ArrayList<Card> mCards = null;
    private DropCardScrollAdapter dropCard = null;
    private CardScrollView mCardScrollView = null;

    @Override
    public void onCreate(Bundle saveInstance) {
        super.onCreate(saveInstance);
        context = this;

        DBProducts dbProducts = new DBProducts(context);
        dbProducts.removeCart();

        mCards = new ArrayList<Card>();
        Card card = new Card(context);
        card.addImage(R.drawable.delete);
        card.setText(R.string.cart_deleted);
        card.setFootnote(R.string.cart_deleted);
        card.setImageLayout(Card.ImageLayout.LEFT);
        mCards.add(card);
        dropCard = new DropCardScrollAdapter(mCards);
        mCardScrollView = new CardScrollView(context);
        mCardScrollView.activate();
        mCardScrollView.setAdapter(dropCard);

        setContentView(mCardScrollView);
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        DropCart.context = context;
    }
    
    
}