package com.wul4.hellompay;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.google.android.glass.app.Card;
import com.wul4.hellompay.basedatos.DBProducts;

public class SelectedCard extends Activity {

    private static Context context = null;
    private float price = 0;
    private String cardSelected = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            price = extras.getFloat("price");
            cardSelected = extras.getString("card");
        }

        // Create a card with some simple text and a footer.
        Card card = new Card(this);
        String cardUsed = null;

        if (cardSelected.contentEquals("Card 1")) {
            cardUsed = "Dark Blue";
            card.addImage(R.drawable.card1);
        }
            
        if (cardSelected.contentEquals("Card 2")) {
            cardUsed = "Golden";
            card.addImage(R.drawable.card2);
        }
            
        if (cardSelected.contentEquals("Card 3")) {
            cardUsed = "Clair blue";
            card.addImage(R.drawable.card3);
        }

        card.setText(getString(R.string.purchase_finished_sucessfully) + ": " + cardUsed + " card");
        card.setFootnote(getString(R.string.total_price) + ": " + price);

        // Don't call this if you're using TimelineManager
        View card1View = card.getView();
        setContentView(card1View);

        DBProducts dbProductos = new DBProducts(context);
        dbProductos.removeCart();
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        SelectedCard.context = context;
    }
}
