package com.wul4.hellompay.basedatos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Helper de BD de Lista de Notificaciones
 * @author JORIDER
 *
 */
public class DatabaseHelper extends SQLiteOpenHelper {
	
	public static String TAG = DatabaseHelper.class.getName();
	
	public static final String DATABASE_NAME = "GLASSDBSTORE";
	public static final int DATABASE_VERSION = 1;
	
	public static final int ERROR_GET_WRITABLE_DB = -2;
	public static final int ERROR_ACCESS_OPEN_HELPER = -3;
	
	public static final String PRODUCTS_TABLE_NAME = "PRODUCTS";
	
	public static final String CREATE_PRODUCTS_TABLE_NAME = "CREATE TABLE IF NOT EXISTS "+PRODUCTS_TABLE_NAME
			+ " (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, description TEXT, price REAL, quantity INTEGER, shopped INTEGER, productId TEXT);";	
			
	public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    	try{
    		db.execSQL(CREATE_PRODUCTS_TABLE_NAME);
            Log.wtf(TAG, "Database creada correctamente");
            
    	}catch(SQLiteException e){
    		Log.wtf(TAG, "Error al crear BD: "+e.getMessage());
    	}
        
    }
    
    
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
        
//    @Override
//    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
//    	Log.wtf(TAG, "perfect!");
//    }
    
}