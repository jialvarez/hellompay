package com.wul4.hellompay.basedatos;

import java.util.ArrayList;

import com.wul4.hellompay.model.Product;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

/**
 * Clase que gestiona la Tabla Products. Tenemos los atributos en el Array columns
 * @author JORIDER
 *
 */
public class DBProducts {

    public static final String TABLE_NAME = DatabaseHelper.PRODUCTS_TABLE_NAME;
    private String[] columns = { "title", "description", "price", "quantity", "shopped", "productId" };
    private static final String TAG = DBProducts.class.getName();

    private SQLiteDatabase db = null;
    @SuppressWarnings("unused")
    private Context context = null;
    private DatabaseHelper openHelper = null;

    /**
     * Constructor de la base de datos de Products
     * @param context
     */
    public DBProducts(Context context) {
        this.context = context;
        //        OperationManager op = ComunOperationManager.getInstancia().getOm();
        openHelper = new DatabaseHelper(context);
    }

    public int addProduct(Product product) {

        long valor = -1;

        try {
            db = openHelper.getWritableDatabase();

            if (product.getProductId() == null) {
                valor = -1;
            } else {
                ContentValues values = new ContentValues();
                values.put(columns[0], product.getTitle());
                values.put(columns[1], product.getDescription());
                values.put(columns[2], product.getPrice());
                values.put(columns[3], product.getQuantity());
                values.put(columns[4], product.getShopped());
                values.put(columns[5], product.getProductId());

                valor = db.insert(TABLE_NAME, null, values);
            }

        } catch (SQLiteException e) {
            Log.e(TAG, e.getMessage());
            valor = DatabaseHelper.ERROR_GET_WRITABLE_DB;

        } catch (NullPointerException e) {
            Log.e(TAG, e.getMessage());
            valor = DatabaseHelper.ERROR_ACCESS_OPEN_HELPER;
        }

        cerrarConector();

        return (int) valor;
    }

    public void getProducts(ArrayList<Product> products) {

        //Limpio la lista actual
        products.clear();

        //Ahora voy a sacar los mensajes de la BD
        try {
            db = openHelper.getReadableDatabase();

            Cursor c = db.query(TABLE_NAME, new String[] { columns[0], columns[1], columns[2], columns[3], columns[4], columns[5] }, null, null, null, null, null, null);
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya mas registros
                do {
                    String title = c.getString(0);
                    String description = c.getString(1);
                    Float price = c.getFloat(2);
                    Integer quantity = c.getInt(3);
                    Integer shopped = c.getInt(4);
                    String productId = c.getString(5);

                    Product product = new Product(title, description, price, quantity, shopped, productId);
                    products.add(product);
                } while (c.moveToNext());
            } else {
                products.clear();
            }
            c.close();

        } catch (SQLiteException e) {
            Log.e(TAG, e.getMessage());

        } catch (NullPointerException e) {
            Log.e(TAG, e.getMessage());
        }

        cerrarConector();
    }

    public void getCartProducts(ArrayList<Product> products) {

        //Limpio la lista actual
        products.clear();

        //Ahora voy a sacar los mensajes de la BD
        try {
            db = openHelper.getReadableDatabase();

            Cursor c = db.query(TABLE_NAME, new String[] { columns[0], columns[1], columns[2], columns[3], columns[4], columns[5] }, "shopped=1", null, null, null, null, null);
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya mas registros
                do {
                    String title = c.getString(0);
                    String description = c.getString(1);
                    Float price = c.getFloat(2);
                    Integer quantity = c.getInt(3);
                    Integer shopped = c.getInt(4);
                    String productId = c.getString(5);

                    Product product = new Product(title, description, price, quantity, shopped, productId);
                    products.add(product);
                } while (c.moveToNext());
            } else {
                products.clear();
            }
            c.close();

        } catch (SQLiteException e) {
            Log.e(TAG, e.getMessage());

        } catch (NullPointerException e) {
            Log.e(TAG, e.getMessage());
        }

        cerrarConector();
    }

    /**
     * Devuelve una product dada un id
     * Si devuelve null, no existe esa product
     * 
     * @param idProduct String
     * @return Product
     */
    public Product getProduct(String idProduct) {

        //Ahora voy a sacar los mensajes de la BD
        Product product = null;
        try {
            db = openHelper.getReadableDatabase();
            Cursor c = db.query(TABLE_NAME, new String[] { columns[0], columns[1], columns[2], columns[3], columns[4], columns[5] }, "productId='" + idProduct + "'", null, null, null, null, null);
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya mas registros
                do {
                    String title = c.getString(0);
                    String description = c.getString(1);
                    Float price = c.getFloat(2);
                    Integer quantity = c.getInt(3);
                    Integer shopped = c.getInt(4);
                    String productId = c.getString(5);

                    product = new Product(title, description, price, quantity, shopped, productId);
                } while (c.moveToNext());
            } else {
                product = null;
            }
            c.close();

        } catch (SQLiteException e) {
            Log.e(TAG, e.getMessage());

        } catch (NullPointerException e) {
            Log.e(TAG, e.getMessage());
        }

        cerrarConector();
        return product;
    }

    /**
     * Actualizar la product completamente, devuelve la cantidad de filas afectadas.
     * Si devuelve 0, no ha actualizado nada, -1 ha dado error al abrir
     * 
     * @param product Product
     */
    public int updateProduct(Product product) {

        int valor = 0;

        try {
            db = openHelper.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(columns[0], product.getTitle());
            values.put(columns[1], product.getDescription());
            values.put(columns[2], product.getPrice());
            values.put(columns[3], product.getQuantity());
            values.put(columns[4], product.getShopped());
            values.put(columns[5], product.getProductId());

            valor = db.update(TABLE_NAME, values, "productId=" + product.getProductId(), null);

        } catch (SQLiteException e) {
            Log.e(TAG, e.getMessage());
            valor = DatabaseHelper.ERROR_GET_WRITABLE_DB;

        } catch (NullPointerException e) {
            Log.e(TAG, e.getMessage());
            valor = DatabaseHelper.ERROR_ACCESS_OPEN_HELPER;
        }

        cerrarConector();
        return valor;

    }

    /**
     * Eliminar el carrito (setear todos los productos a 0 en atributo shopped)
     * 
     * @param product Product
     */
    public int removeCart() {

        int valor = 0;

        db = openHelper.getWritableDatabase();
        valor = db.delete(TABLE_NAME, null, null);

        cerrarConector();
        return valor;

    }
    
    public float getTotalCartPrice() {
        float totalPrice = 0.0f;

        //Ahora voy a sacar los mensajes de la BD
        try {
            db = openHelper.getReadableDatabase();

            Cursor c = db.query(TABLE_NAME, new String[] { columns[0], columns[1], columns[2], columns[3], columns[4], columns[5] }, "shopped=1", null, null, null, null, null);
            if (c.moveToFirst()) {
                //Recorremos el cursor hasta que no haya mas registros
                do {
                    Float price = c.getFloat(2);
                    totalPrice += price;

                } while (c.moveToNext());
            } 
            c.close();

        } catch (SQLiteException e) {
            Log.e(TAG, e.getMessage());

        } catch (NullPointerException e) {
            Log.e(TAG, e.getMessage());
        }

        cerrarConector();
        return totalPrice;
    }


    /**
     * Metodo para cerrar la conexion con BD
     */
    public void cerrarConector() {
        if (db.isOpen())
            db.close();
    }

}