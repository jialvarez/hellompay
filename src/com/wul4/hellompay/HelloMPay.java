package com.wul4.hellompay;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.wul4.hellompay.basedatos.DBProducts;
import com.wul4.hellompay.model.Product;

import es.wul4.qrar.client.android.CaptureActivity;

public class HelloMPay extends Activity {

    public static final int REQUEST_CODE = 42;
    private static final int RETURN_RESULT = 1;
    private static final String RETURN_TAG = "result";
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;

        initializeProducts();

        Intent comprar = new Intent(context, CaptureActivity.class);
        startActivityForResult(comprar, 0);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RETURN_RESULT) {
                String productId = data.getStringExtra(RETURN_TAG);

                Intent intent = new Intent(HelloMPay.this, ViewProduct.class);
                Bundle extras = new Bundle();
                extras.putString("productId", productId);
                intent.putExtras(extras);
                startActivityForResult(intent, 1);
                
            } else {
            	// han cancelado el escaneado de qr
            	finish();
            }
        } else if (requestCode == 1) {
        	if (resultCode == Activity.RESULT_CANCELED) {
        		// volvemos de ViewProduct con resultado: volver a escanear
        		Intent comprar = new Intent(context, CaptureActivity.class);
                startActivityForResult(comprar, 0);
            } 
        } else {
        	finish();
        }
    }

    private void initializeProducts() {
        DBProducts dbProducts = new DBProducts(context);
        ArrayList<Product> products = new ArrayList<Product>();
        dbProducts.getProducts(products);

        if (products.size() == 0) {
            Product product;
            product = new Product("Galaxy Nexus", "Android 4.0\nSuper Amoled 720p Screen\n1.2 GHz Dual Core CPU", (float) 400.00, 1, 0, "1000");
            dbProducts.addProduct(product);
            product = new Product("Ice cream", "Mmm, what an ice cream! Enjoy your favourite flavour, strawberry, cream, lemon, chocolate...", (float) 3.00, 1, 0, "2000");
            dbProducts.addProduct(product);
            product = new Product("Pizza", "Tomato\nMozzarella\nPepperoni\nRed pepper\nExtra cheese\nLove", (float) 15.00, 1, 0, "3000");
            dbProducts.addProduct(product);
            product = new Product("Book", "The Everyday Paleo Family Cookbook offers guidance on how to bring your family together with the magic of real food.", (float) 30.00, 1, 0, "4000");
            dbProducts.addProduct(product);
            product = new Product("Movie tickets", "Buy your movie tickets for your cinema. Last movie is waiting for you! Don't forget pop corn and drinks ;-)", (float) 40.00, 1, 0, "5000");
            dbProducts.addProduct(product);
        }
    }
}