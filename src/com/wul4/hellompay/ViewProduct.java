package com.wul4.hellompay;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollView;
import com.wul4.hellompay.baseadapter.ExampleCardScrollAdapter;
import com.wul4.hellompay.basedatos.DBProducts;
import com.wul4.hellompay.model.Product;
import com.wul4.hellompay.util.Util;

public class ViewProduct extends Activity {
    private Context context;
    private ArrayList<Card> mCards;
    private CardScrollView mCardScrollView;
    private static final int MENU_ITEM_ITEM1 = 1;
    private static final int RETURN_RESULT = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;

        DBProducts dbProducts = new DBProducts(context);

        Intent intent = getIntent();
        String productId = intent.getStringExtra("productId");
        Product product = dbProducts.getProduct(productId);

        createProductCard(product);

        mCardScrollView = new CardScrollView(this);
        ExampleCardScrollAdapter adapter = new ExampleCardScrollAdapter(mCards, context, productId, -1);
        mCardScrollView.setAdapter(adapter);
        mCardScrollView.activate();
        setContentView(mCardScrollView);
        mCardScrollView.setOnItemClickListener(adapter);
    }
    
    @Override
    public void onBackPressed() {
    	super.onBackPressed();
    	Intent intent = new Intent();
    	setResult(RETURN_RESULT, intent);
    }

    private void createProductCard(Product product) {
        mCards = new ArrayList<Card>();

        Card card = null;

        card = new Card(this);
        card.setText(product.getDescription());
        card.setFootnote(product.getQuantity() + " " + product.getTitle() + "  $" + product.getPrice() + "0");
        card.setImageLayout(Card.ImageLayout.LEFT);

        card.addImage(Util.getProductImage(product.getProductId()));

        mCards.add(card);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, MENU_ITEM_ITEM1, Menu.NONE, "Agregar al carrito");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case MENU_ITEM_ITEM1:

            return true;

        default:
            return false;
        }
    }
}
