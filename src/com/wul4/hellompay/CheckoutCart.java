package com.wul4.hellompay;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.google.android.glass.app.Card;
import com.google.android.glass.widget.CardScrollView;
import com.wul4.hellompay.baseadapter.EmptyCheckoutScrollAdapter;
import com.wul4.hellompay.baseadapter.ExampleCardScrollAdapter;
import com.wul4.hellompay.basedatos.DBProducts;
import com.wul4.hellompay.model.Product;
import com.wul4.hellompay.util.Util;

public class CheckoutCart extends Activity {

    private static Context context = null;
    private ArrayList<Card> mCards = null;
    private CardScrollView mCardScrollView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;

        DBProducts dbProducts = new DBProducts(context);
        ArrayList<Product> products = new ArrayList<Product>();

        float price = 0;
        
        dbProducts.getCartProducts(products);

        mCards = new ArrayList<Card>();
        for (Product product : products) {
            price = price + product.getPrice();
            Card card = new Card(context);
            card.setFootnote(product.getQuantity() + " " + product.getTitle() + "  $" + product.getPrice() + "0");
            card.setText(product.getDescription());
            card.setImageLayout(Card.ImageLayout.LEFT);
            card.addImage(Util.getProductImage(product.getProductId()));
            mCards.add(card);
        }

        if (products.size() > 0) {
            mCardScrollView = new CardScrollView(this);
            price = dbProducts.getTotalCartPrice();
            if (price == 0) price = -1;
            ExampleCardScrollAdapter adapter = new ExampleCardScrollAdapter(mCards, context, null, price);
            mCardScrollView.setAdapter(adapter);
            mCardScrollView.activate();
            setContentView(mCardScrollView);
            mCardScrollView.setOnItemClickListener(adapter);

        } else {
            mCards = new ArrayList<Card>();
            Card card = new Card(context);
            card.addImage(R.drawable.delete);
            card.setText(R.string.no_products_added);
            card.setFootnote(R.string.no_products_added);
            card.setImageLayout(Card.ImageLayout.LEFT);
            mCards.add(card);
            EmptyCheckoutScrollAdapter dropCard = new EmptyCheckoutScrollAdapter(mCards);
            mCardScrollView = new CardScrollView(context);
            mCardScrollView.activate();
            mCardScrollView.setAdapter(dropCard);

            setContentView(mCardScrollView);
        }
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        CheckoutCart.context = context;
    }

}
