package com.wul4.hellompay.model;

public class Product {

    private String title;
    private String description;
    private float price;
    private int quantity;
    private int shopped;
    private String productId;

    public Product(String title, String description, float price, int quantity, int shopped, String productId) {
        super();
        this.title = title;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.shopped = shopped;
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getShopped() {
        return shopped;
    }

    public void setShopped(int shopped) {
        this.shopped = shopped;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
