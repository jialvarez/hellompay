/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wul4.hellompay;

import com.wul4.hellompay.basedatos.DBProducts;
import com.wul4.hellompay.model.Product;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

/**
 * Activity showing the options menu.
 */
public class MenuActivity extends Activity {

    private Context context;
    private DBProducts dbProducts;
    private Product product;
    private boolean isAttached = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;
        dbProducts = new DBProducts(context);

        Intent intent = getIntent();
        String productId = intent.getStringExtra("productId");
        product = dbProducts.getProduct(productId);
    }
    
    @Override
    public void onAttachedToWindow() {
      super.onAttachedToWindow();
      this.isAttached = true;
      openOptionsMenu();
    }

    @Override
    public void onResume() {
    	android.os.Debug.waitForDebugger();
        super.onResume();
        if (this.isAttached)
        	openOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection.
        switch (item.getItemId()) {
        case R.id.add_product:
            product.setShopped(1);
            dbProducts.updateProduct(product); 
            break;
        default:
            return super.onOptionsItemSelected(item);
        }
        
        return true;
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        // Nothing else to do, closing the Activity.
        finish();
    }
}
